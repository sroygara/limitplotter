#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import ROOT as root
from os.path import exists
from ROOT import *
import array as array
gStyle.SetOptStat(0)
gROOT.SetBatch(kTRUE)

 

###################################
# Notes
#
# Need to change the way the trex directories are named to include sint tanb
# and mX
#
# If time, could prompt user to input these variables.
#
##################################

# These will be the fixed variables for the scan.
plotType_global='mX'
plot_sint_ma350_ref=False
plot_sint_ma200_ref=False
ma_fixed = 250
sint_fixed=0.35
tanb_fixed=1
mX_fixed=10
mA_fixed=600
sep_based = True

# Tell me where to find things...
#input_folder="/eos/user/s/sroygara/2HDMa/postProd-samples/fit/DM_scan/fit/Sep_Based/high_mass/"
input_folder="/eos/user/s/sroygara/2HDMa/postProd-samples/fit/DM_scan/fit/mH_based/low_mass/"
#input_folder="/eos/user/s/sroygara/2HDMa/postProd-samples/fit/sinp_scan/fit/BDT_based/SinpScan_ma200_mH600_BDTBased_v1/"
#input_folder="/eos/home-s/sroygara/2HDMa/postProd-samples/fit/ma_mA_scan/sinp07/fit/Sep_based/"
fit_file="myLimit_CL95.root"
xsec_file_path='./xsec.txt' #structured as "ma mA xsec[pb]" -- needs updating to include sint tanb mX

bool_plotXsec = False

# This is the default list of all possible parameters
ma_list = [100,200,250,300,350,375,400,500,600,700,800]
mA_list = [200, 400, 500, 600, 700, 800, 900, 1000, 1200]
tanb_list = [0.3,0.5,1.0,3.0,5.0]
sint_list = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
mX_list = [10,100,120,125,130,150,200,250,300,350,400,450,500] # this is not right ?

# Y axis limits
ymax_limit=50
ymin_limit=0.1
ymax_Xsec=1
ymin_Xsec=0.05

def makeAtlasStyle(parameters):
  # split the parameters line its too long
  pars_list = parameters.split(", ")
  
  Latex1 = TLatex(0.15,0.88,"ATLAS #bf{"+"Internal"+"}")
  Latex2 = TLatex(0.15,0.83,"#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
  Latex3 = TLatex(0.15,0.78,"#bf{BSM 4tops SSML}")
  Latex4 = TLatex(0.15,0.73,"#bf{"+ pars_list[0] + ", "+ pars_list[1] + "}")
  Latex5 = TLatex(0.15,0.68,"#bf{"+ pars_list[2] + ", "+ pars_list[3] +  "}")

  Latex1.SetNDC() # make it abosulte position
  Latex2.SetNDC()
  Latex3.SetNDC()
  Latex4.SetNDC()
  Latex5.SetNDC()

  Latex1.SetTextSize(0.04)
  Latex2.SetTextSize(0.04)
  Latex3.SetTextSize(0.04)
  Latex4.SetTextSize(0.04)
  Latex5.SetTextSize(0.04)
  return Latex1, Latex2, Latex3, Latex4, Latex5

def makePad():

  pad = TPad("main","main",0,0.05,1,1.0)

  pad.SetTopMargin(0.05)
  pad.SetRightMargin(0.05)
  pad.SetLeftMargin(0.12)

  pad.Draw()

  return pad

def getLimitInfo(ma, mA, sint, tanb, mX, xvar):
  limit_data=[]
  sint = str(sint).replace(".","")
  #sint = str(sint).replace(".","") 
  tanb = str(tanb).replace(".","")
  #format directory name
  directory="FourTop_BDTFit_2HDMa_Comb_2hdm_tttt_ma" + str(ma) + "_MH" + str(mA) + "_mX" + str(mX) + "_mXscan" + "_sinp" + str(sint) + "_tanb" + str(tanb)+ "_fit_v212120_FullSyst_v12_finalBDT_sherpa2210_ttWQCDEW_deco_DDQmisID_COCR_2D_rates_Final_EWXsec20"
  path = input_folder+directory+"/Limits/asymptotics/"+fit_file
  if exists(path) :
    f = TFile(path)
    stats = f.Get("stats")
    stats.GetEntry(0)

    limit_data.append(stats.GetLeaf("obs_upperlimit").GetValue(0))
    limit_data.append(stats.GetLeaf("exp_upperlimit").GetValue(0))
    limit_data.append(stats.GetLeaf("exp_upperlimit_plus1").GetValue(0))
    limit_data.append(stats.GetLeaf("exp_upperlimit_plus2").GetValue(0))
    limit_data.append(stats.GetLeaf("exp_upperlimit_minus1").GetValue(0))
    limit_data.append(stats.GetLeaf("exp_upperlimit_minus2").GetValue(0))
    limit_data.append(xvar)
  else:
    print("Could not find directory:" + directory)
  return limit_data

# This needs to be fixed to include the fact that I added tanb, sint, mX and ma scans
def getXsecInfo(ma, mAlist):

  xsec_data = []
  file_data = []

  if exists(xsec_file_path):
    file_data = np.loadtxt(xsec_file_path)

    # Get all indices of ma value occurences in list
    ma_index_list = [ i for i, x in enumerate(file_data[:,0]) if x == ma ]

    for mA in mAlist:
      # Find which index corresponds to the mA value
      for index in ma_index_list:
        if file_data[index,1] == mA:


          xsec_data.append(file_data[index,2])

    return xsec_data #Returns all xsec values for mA

# This is the main (and only) function that does the plotting
def makePlot(plotType="", plotXsec=False):

  # These if statements set all plotType specific variables

  if plotType=="tanb":

    logY=True
    ymax_plot = ymax_limit # Y axis max
    ymin_plot = ymin_limit # Y axis min
    Ytitle = "#mu"
    Xtitle = "tan(#beta)" # maybe simpler to do GeV
    xax2_style="U-"
    yax2_style="U+"
    plot_file_name="LimitPlot_tanbScan_ma" +str(ma_fixed)+"_mA" + str(mA_fixed) + "_sint" + str(sint_fixed).replace('.','p') + "_mX" + str(mX_fixed)+     ".pdf"
    parameters = "ma = " + str(ma_fixed) + " GeV, mA = " + str(mA_fixed) + " GeV, sin#theta = " + str(sint_fixed)+ ", mX = " + str(mX_fixed) + " GeV" 

    print(">>> Gathering Data")
    limit_data_list=[]
    for tanb in tanb_list:
        if getLimitInfo(ma_fixed, mA_fixed, sint_fixed, tanb, mX_fixed, tanb) != [] :
          limit_data_list.append(getLimitInfo(ma_fixed, mA_fixed, sint_fixed, tanb, mX_fixed, tanb))
    x = np.array([float(item[6]) for item in limit_data_list])

  elif plotType=="sint":

    logY=True
    ymax_plot = ymax_limit # Y axis max
    ymin_plot = ymin_limit # Y axis min
    Ytitle = "#sigma/#sigma_{theory}"
    Xtitle = "sin#theta"
    xax2_style="U-"
    yax2_style="U+"
    if sep_based==True:
      plot_file_name="LimitPlot_sintScan_SepBased_ma" +str(ma_fixed)+"_mA" + str(mA_fixed) + "_tanb" +str(tanb_fixed).replace('.','p') + "_mX" + str(mX_fixed)+".pdf"
      tgraph_file_name="LimitPlot_sintScan_SepBased_ma" +str(ma_fixed)+"_mA" + str(mA_fixed) + "_tanb" +str(tanb_fixed).replace('.','p') + "_mX" + str(mX_fixed)+".root"
    else:
      plot_file_name="LimitPlot_sintScan_ma" +str(ma_fixed)+"_mA" + str(mA_fixed) + "_tanb" +str(tanb_fixed).replace('.','p') + "_mX" + str(mX_fixed)+".pdf"
      tgraph_file_name="LimitPlot_sintScan_ma" +str(ma_fixed)+"_mA" + str(mA_fixed) + "_tanb" +str(tanb_fixed).replace('.','p') + "_mX" + str(mX_fixed)+".root"
    parameters = "m_{a} = " + str(ma_fixed) + " GeV, m#kern[-0.15]{_{A}} = " + str(mA_fixed) + " GeV"+ ", tan#beta = " + str(tanb_fixed) + ", m#kern[-0.15]{_{#chi}} = " + str(mX_fixed) + " GeV" 
    
    print(">>> Gathering Data")
    limit_data_list=[]
    for sint in sint_list:
        if getLimitInfo(ma_fixed, mA_fixed, sint, tanb_fixed, mX_fixed, sint) != [] :
          limit_data_list.append(getLimitInfo(ma_fixed, mA_fixed, sint, tanb_fixed, mX_fixed, sint))
    x = np.array([float(item[6]) for item in limit_data_list])

  elif plotType=="mX":

    logY=True
    ymax_plot = ymax_limit # Y axis max
    ymin_plot = ymin_limit # Y axis min
    Ytitle = "#sigma/#sigma_{theory}"
    Xtitle = "m_{#chi} [GeV]" # maybe simpler to do GeV
    xax2_style="U-"
    yax2_style="U+"
    plot_file_name="LimitPlot_mXScan_zoom_ma" +str(ma_fixed)+"_mA" + str(mA_fixed) + "_sint" + str(sint_fixed).replace('.','p')+ "_tanb" +str(tanb_fixed).replace('.','p') + ".pdf"
    tgraph_file_name="LimitPlot_mXScan_ma" +str(ma_fixed)+"_mA" + str(mA_fixed) + "_sint" + str(sint_fixed).replace('.','p')+ "_tanb" +str(tanb_fixed).replace('.','p') + ".root"
    parameters = "ma = " + str(ma_fixed) + " GeV, mA = " + str(mA_fixed) + " GeV, sin#theta = " + str(sint_fixed)+ ", tan#beta = " + str(tanb_fixed) 

    print(">>> Gathering Data")
    limit_data_list=[]
    for mX in mX_list:
        if getLimitInfo(ma_fixed, mA_fixed, sint_fixed, tanb_fixed, mX, mX) != [] :
          limit_data_list.append(getLimitInfo(ma_fixed, mA_fixed, sint_fixed, tanb_fixed, mX, mX))
    x = np.array([float(item[6]) for item in limit_data_list])

  elif plotType=="mA":

    logY=False
    ymax_plot = ymax_limit # Y axis max
    ymin_plot = ymin_limit # Y axis min
    Ytitle = "#mu"
    Xtitle = "m_{A} [GeV]" # maybe simpler to do GeV
    xax2_style="U-"
    yax2_style="U+"
    plot_file_name="LimitPlot_mA.pdf"
    plot_file_name="LimitPlot_mAScan_ma" +str(ma_fixed) + "_sint" + str(sint_fixed).replace('.','p')+ "_tanb" +str(tanb_fixed).replace('.','p') + "_mX" + str(mX_fixed)+ ".pdf"
    tgraph_file_name="LimitPlot_mAScan_ma" +str(ma_fixed) + "_sint" + str(sint_fixed).replace('.','p')+ "_tanb" +str(tanb_fixed).replace('.','p') + "_mX" + str(mX_fixed)+ ".root"
    parameters = "ma = " + str(ma_fixed) + " GeV, sin#theta = " + str(sint_fixed)+ ", tan#beta = " + str(tanb_fixed) + ", mX = " + str(mX_fixed) + " GeV" 
    
    print(">>> Gathering Data")
    limit_data_list=[]
    for mA in mA_list:
        if getLimitInfo(ma_fixed, mA, sint_fixed, tanb_fixed, mX_fixed, mA) != [] :
          limit_data_list.append(getLimitInfo(ma_fixed, mA, sint_fixed, tanb_fixed, mX_fixed, mA))
    x = np.array([float(item[6]) for item in limit_data_list])
  elif plotType=="ma":

    logY=True
    ymax_plot = ymax_limit # Y axis max
    ymin_plot = ymin_limit # Y axis min
    Ytitle = "#mu"
    Xtitle = "m_{a} [GeV]" # maybe simpler to do GeV
    xax2_style="U-"
    yax2_style="U+"
    plot_file_name="LimitPlot_maScan_mA" + str(mA_fixed) + "_sint" + str(sint_fixed).replace('.','p')+ "_tanb" +str(tanb_fixed).replace('.','p') + "_mX" + str(mX_fixed)+ ".pdf"
    parameters = "mA = " + str(mA_fixed) + " GeV, sin#theta = " + str(sint_fixed)+ ", tan#beta = " + str(tanb_fixed) + ", mX = " + str(mX_fixed) + " GeV" 

    print(">>> Gathering Data")
    limit_data_list=[]
    for ma in ma_list:
        if getLimitInfo(ma, mA_fixed, sint_fixed, tanb_fixed, mX_fixed, ma) != [] :
          limit_data_list.append(getLimitInfo(ma, mA_fixed, sint_fixed, tanb_fixed, mX_fixed, ma))
    x = np.array([float(item[6]) for item in limit_data_list])

  if plotXsec==True:

    logY=True
    ymax_plot = ymax_Xsec # Y axis max
    ymin_plot = ymin_Xsec # Y axis min
    Ytitle = "#sigma(pp#rightarrow t#bar{t}H/A/a)#times BR(H/A/a#rightarrow t#bar{t}) [pb]"
    xax2_style="U-"
    yax2_style="UG+"
    plot_file_name="LimitOnXsecPlot.pdf"

  # From here onward,  no plotType dependent info is hard coded
  print(">>> Making your plot")
  Limit = TCanvas("Limit","Limit",200,10,600,500)
  Limit = TCanvas()
  Limit.cd()

  # Make pad
  pad = makePad()
  pad.cd() 
  # Make logy for xsec limit plot
  if logY==True: pad.SetLogy()
  pad.Update()
  # Make multigraph and resize
  mg = TMultiGraph()
  mg.SetMaximum(ymax_plot)
  mg.SetMinimum(ymin_plot)
  pad.Update()
  print(ymin_plot)
  # Label axes
  mg.GetXaxis().SetTitle(Xtitle)
  mg.GetYaxis().SetTitle(Ytitle)
  gStyle.SetTitleSize(20,"y")
  gStyle.SetTitleSize(20,"x")
  gStyle.SetTitleFont(43,"y")
  gStyle.SetTitleFont(43,"x")
  gStyle.SetTitleOffset(1,"y")

  obs_y = np.array([item[0] for item in limit_data_list])
  exp_y = np.array([item[1] for item in limit_data_list])

  ZeroErrorList = []
  for i in range(len(x)):
    ZeroErrorList.append(0)
  OneSigma_x_left = np.array(ZeroErrorList)
  OneSigma_y_plus = np.array([item[2] for item in limit_data_list])
  OneSigma_x_right = np.array(ZeroErrorList)
  OneSigma_y_minus = np.array([item[4] for item in limit_data_list])
  TwoSigma_x_left = np.array(ZeroErrorList)
  TwoSigma_y_plus = np.array([item[3] for item in limit_data_list])
  TwoSigma_x_right = np.array(ZeroErrorList)
  TwoSigma_y_minus = np.array([item[5] for item in limit_data_list])

  # Rescale data points to limit on xsec
  if plotXsec==True:
    xsec_list = getXsecInfo(ma_list, mA_list)
    for i in range(len(OneSigma_y_plus)):
      OneSigma_y_plus[i] = OneSigma_y_plus[i]*xsec_list[i]
      TwoSigma_y_plus[i] = TwoSigma_y_plus[i]*xsec_list[i]
      OneSigma_y_minus[i] = OneSigma_y_minus[i]*xsec_list[i]
      TwoSigma_y_minus[i] = TwoSigma_y_minus[i]*xsec_list[i]
      obs_y[i] = obs_y[i]*xsec_list[i]
      exp_y[i] = exp_y[i]*xsec_list[i]

  
  
  # This is just for final TGraph submission
  ge_high_final = TGraph(len(x), x, OneSigma_y_plus)
  ge_low_final = TGraph(len(x), x, OneSigma_y_minus)
  ge2_high_final = TGraph(len(x), x, TwoSigma_y_plus)
  ge2_low_final = TGraph(len(x), x, TwoSigma_y_minus)
  
  
  
  for i in range(len(OneSigma_y_plus)):
    OneSigma_y_plus[i] = OneSigma_y_plus[i] - exp_y[i]
    TwoSigma_y_plus[i] = TwoSigma_y_plus[i] - exp_y[i]
    OneSigma_y_minus[i] = OneSigma_y_minus[i] - exp_y[i]
    TwoSigma_y_minus[i] = TwoSigma_y_minus[i] - exp_y[i]

  # Make and plot error bars
  
  x = np.array(x,'double')
  OneSigma_x_left = np.array(OneSigma_x_left, 'double')
  OneSigma_x_right = np.array(OneSigma_x_right, 'double')
  OneSigma_y_plus = np.array(OneSigma_y_plus, 'double')
  OneSigma_y_minus = np.array(OneSigma_y_minus, 'double')
 
  TwoSigma_x_left  = np.array(TwoSigma_x_left, 'double')
  TwoSigma_x_right = np.array(TwoSigma_x_right, 'double')
  TwoSigma_y_plus  = np.array(TwoSigma_y_plus, 'double')
  TwoSigma_y_minus = np.array(TwoSigma_y_minus, 'double')
  

  ge = TGraphAsymmErrors(len(x), x, exp_y, OneSigma_x_left, OneSigma_x_right, -OneSigma_y_minus, OneSigma_y_plus)
  ge.SetFillColor(kGreen)

  ge2 = TGraphAsymmErrors(len(x), x, exp_y, TwoSigma_x_left, TwoSigma_x_right, -TwoSigma_y_minus, TwoSigma_y_plus)
  ge2.SetFillColorAlpha(kYellow, 0.5);

  pad.Update()
  print(">>> Plotting sigma")
  mg.Add(ge)
  mg.Add(ge2)
  mg.Draw("A3")
  
  pad.Update()
  # Make and plot observed limit
  print(">>> Plotting mu obs")
  gr_obs = TGraph(len(x),x,obs_y)
  gr_obs.SetLineColor(kBlack)
  gr_obs.SetLineStyle(kSolid)
  gr_obs.SetMarkerSize(0.9)
  gr_obs.SetMarkerStyle(20)
  gr_obs.SetLineWidth(3);
  gr_obs.SetFillStyle(0);
  gr_obs.Draw("L SAME")

  pad.Update()
  # Make and plot expected limit
  gr_exp = TGraph(len(x),x,exp_y)
  gr_exp.SetLineColor(kBlack)
  gr_exp.SetLineStyle(kDashed)
  gr_exp.SetMarkerSize(0.2)
  gr_exp.SetLineWidth(4);
  gr_exp.SetFillStyle(0);
  gr_exp.Draw("SAME")
  pad.Update()
 
  if plot_sint_ma200_ref == True:
    old_exp_g = TGraph()
    old_exp = TFile("gr40_stA_exp.root")
    old_exp.GetObject("Graph",old_exp_g)
    old_exp_g.SetLineStyle(kDashed)
    old_exp_g.SetLineColor(kRed)
    old_exp_g.SetMarkerSize(0.2)
    old_exp_g.SetLineWidth(4);
    old_exp_g.SetFillStyle(0)
    old_exp_g.Draw("LP SAME")
    
    old_obs_g = TGraph()
    old_obs = TFile("gr40_stA_obs.root")
    old_obs.GetObject("Graph",old_obs_g)
    old_obs_g.SetLineColor(kRed)
    old_obs_g.SetMarkerColor(kRed)
    old_obs_g.SetLineStyle(kSolid)
    old_obs_g.SetMarkerSize(0.9)
    old_obs_g.SetMarkerStyle(20)
    old_obs_g.SetLineWidth(3);
    old_obs_g.SetFillStyle(0)
    old_obs_g.Draw("LP SAME")
  
    mg.GetXaxis().SetLimits(0,1)
 
  if plot_sint_ma350_ref == True:
    old_exp_g = TGraph()
    old_exp = TFile("gr40_stB_exp.root")
    old_exp.GetObject("Graph",old_exp_g)
    old_exp_g.SetLineStyle(kDashed)
    old_exp_g.SetLineColor(kRed)
    old_exp_g.SetMarkerSize(0.2)
    old_exp_g.SetLineWidth(4);
    old_exp_g.SetFillStyle(0)
    old_exp_g.Draw("LP SAME")
    
    old_obs_g = TGraph()
    old_obs = TFile("gr40_stB_obs.root")
    old_obs.GetObject("Graph",old_obs_g)
    old_obs_g.SetLineColor(kRed)
    old_obs_g.SetMarkerColor(kRed)
    old_obs_g.SetLineStyle(kSolid)
    old_obs_g.SetMarkerSize(0.9)
    old_obs_g.SetMarkerStyle(20)
    old_obs_g.SetLineWidth(3);
    old_obs_g.SetFillStyle(0)
    old_obs_g.Draw("LP SAME")
  
    mg.GetXaxis().SetLimits(0,1)
  

  # Make ATLAS style
  Line1, Line2, Line3, Line4, Line5 = makeAtlasStyle(parameters)
  Line1.Draw("SAME")
  Line2.Draw("SAME")
  Line3.Draw("SAME")
  Line4.Draw("SAME")
  Line5.Draw("SAME")

  # Make and plot axes on right and top
  pad.Update()
  axis1 = gr_exp.GetXaxis()
  axis2 = gr_exp.GetYaxis()
  
  xmin = pad.GetUxmin() 
  xmax = pad.GetUxmax() 
  
  xpos = xmax 
  ypos = ymax_plot
  x_axis2 = TGaxis(xmin,ypos,xmax,ypos,xmin,xmax,510,xax2_style)
  x_axis2.SetName("x_axis2");
  x_axis2.SetLabelFont(42)
  x_axis2.Draw("same")
  
  if logY == True:
    y_axis2 = TGaxis(xpos,ymin_plot,xpos,ymax_plot,ymin_plot,ymax_plot,510,"G"+yax2_style)
  else:
    y_axis2 = TGaxis(xpos,ymin_plot,xpos,ymax_plot,ymin_plot,ymax_plot,510,yax2_style)
  y_axis2.SetName("y_axis2");
  y_axis2.SetLabelFont(42)
  y_axis2.SetLabelSize(0.07)
  y_axis2.Draw("same")
  
  axis1.SetTickLength(0.03);
  axis2.SetTickLength(0.03);
  x_axis2.SetTickLength(0.03);
  y_axis2.SetTickLength(0.03);

  # Make and plot legend
  print(">>> Plotting Legend")
  legend3 = TLegend(0.80,0.75,0.94,0.94)
  legend3.SetBorderSize(0)
  legend3.SetNColumns(1);
   
  if plot_sint_ma200_ref == True or plot_sint_ma350_ref == True:
    legend3 = TLegend(0.65,0.60,0.94,0.94)
    legend3.SetBorderSize(0)
    legend3.SetNColumns(1);  
    legend3.AddEntry(old_exp_g,"Expected 36 fb^{-1}","lp")
    legend3.AddEntry(old_obs_g,"Observed 36 fb^{-1}","lp")
  else:
    legend3 = TLegend(0.70,0.70,0.94,0.92)
    legend3.SetBorderSize(0)
    legend3.SetNColumns(1);
  
  legend3.AddEntry(gr_exp,"Expected Limit","l")
  legend3.AddEntry(gr_obs,"Observed Limit","l")
  legend3.AddEntry(ge,"#pm 1 #sigma","f")
  legend3.AddEntry(ge2,"#pm 2 #sigma","f")
  
  
  
  legend3.Draw("SAME")
  # Remaking sigma bands as lines for final TGraph submission
  saveFile = TFile(tgraph_file_name,"RECREATE")
  gr_obs.Write("Obs_0")
  gr_exp.Write("Exp_0")
  ge.Write("Band_1s_0")
  ge2.Write("Band_2s_0")
  ge_high_final.Write("Band_1s_high_0")
  ge_low_final.Write("Band_1s_low_0")
  ge2_high_final.Write("Band_2s_high_0")
  ge2_low_final.Write("Band_2s_low_0")
  saveFile.Close()
  # Save plot
  #Limit.SaveAs("/afs/cern.ch/user/s/sroygara/2hdma-fit/4top/makeplots/" + tgraph_file_name)
  Limit.SaveAs("/afs/cern.ch/user/s/sroygara/2hdma-fit/4top/makeplots/" + plot_file_name)
  
  


def main():
    makePlot(plotType_global,bool_plotXsec)

if __name__ == '__main__':

  main()
